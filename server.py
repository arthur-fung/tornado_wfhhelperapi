import tornado.web
import tornado.ioloop
from tornado.ioloop import IOLoop
from graphene_tornado.schema import schema
from graphene_tornado.tornado_graphql_handler import TornadoGraphQLHandler
import json
from graphql.error import GraphQLError
from graphql.error import format_error as format_graphql_error
from tornado.escape import json_decode, json_encode
from contacts import *

class TornadoGraphQLHandler(tornado.web.RequestHandler):
    def post(self):
        return self.handle_graqhql()

    def handle_graqhql(self):
        result = self.execute_graphql()
        response = {'data': result.data}
        self.write(json_encode(response))

    def execute_graphql(self):
        graphql_req = self.graphql_request
        return self.schema.execute(
            graphql_req.get('query'),
            variable_values=graphql_req.get('variables'),
            operation_name=graphql_req.get('operationName'),
            context_value=graphql_req.get('context'),
            middleware=self.middleware
        )

    @property
    def graphql_request(self):
        return json_decode(self.request.body)

    @property
    def content_type(self):
        return self.request.headers.get('Content-Type', 'text/plain').split(';')[0]

    @property
    def schema(self):
        raise NotImplementedError('schema must be provided')

    @property
    def middleware(self):
        return []

    @property
    def context(self):
        return None


class authRequestHandler(tornado.web.RequestHandler):
    def options(self, *args):
        self.set_status(204)
        self.finish()
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Content-type', 'application/json;charset=utf-8')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Max-Age', 2592000)
    def post(self):
        print "Auth Pass"
        data =  tornado.escape.json_decode(self.request.body)
        username = self.get_argument('username', 'No data received')
        password = self.get_body_argument('password', 'No data received')
        # print data
        # print username
        # print password 
        self.write("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJOZXRDUk1Mb2dpbiI6ImFydGh1cmYiLCJlbWFpbCI6IkFydGh1cl9GdW5nQGxpdmluZ3N0b25lLmNvbS5hdSIsIm5hbWUiOiJBcnRodXIgRnVuZyIsImlzQWRtaW4iOiJ0cnVlIiwibmJmIjoxNjQ2OTk3NTc2LCJleHAiOjE2NDcwMDI5NzYsImlhdCI6MTY0Njk5NzU3Nn0.XGg42R8Yzj6ihV6kRoyg1yIjiY2biuAnbKNYAuhoki8")

class basicRequestHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world!!!!!!")

class staticRequestHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")

class contactRequestHandler(tornado.web.RequestHandler):
    def options(self, *args):
        self.set_status(204)
        self.finish()
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Content-type', 'application/json;charset=utf-8')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Max-Age', 2592000)
    def post(self):
        self.write('some post')
    def get(self):      
        self.write(json.dumps(contacts))

class contactsListRequestHandler(tornado.web.RequestHandler):
    def options(self, *args):
        self.set_status(204)
        self.finish()
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Content-type', 'application/json;charset=utf-8')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Max-Age', 2592000)
    def post(self):
        self.write('some post')
    def get(self):      
        self.write(json.dumps(contactsList))

if __name__ == "__main__":
    app = tornado.web.Application([
        (r"/", basicRequestHandler),
        (r"/index", staticRequestHandler),
        (r"/contacts", contactsListRequestHandler),
        (r"/staff", contactRequestHandler),
        (r"/auth", authRequestHandler),
        (r'/graphql', TornadoGraphQLHandler, dict(graphiql=True, schema=schema)),
    ])

    app.listen(8881)
    print("I'm listening on port 8881        http://localhost:8881/")
    tornado.ioloop.IOLoop.current().start()